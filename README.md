# module.test

C++20 modules test with scons based build files.

This is mostly a test of building C++20 modules by using scons, and determining
what problems one might come across. It does work, with some caveats:

The names of modules must match the .cpp files they are found in. This can be
avoided by typing "scons headers && scons" to force a kind of dependency
pre-pass.

Modules don't necessarily need to be named after the file they're in, so a
pre-pass is used to scan files and build up this information. Some simple
json files are created which are pulled into the module mapper file, which
is required by gcc.

Modules being imported must be compiled before the object importing them.
SCons must be informed of object dependencies for that to work, and the
pre-pass is also used to gather all of this information. Unfortunately,
this is done in a previous build step, which cannot be injected into the
dependency hierarchy directly because scons first builds the dependency tree
and only then issues the build commands. This is what is requiring module
names to match .cpp files - so that the dependencies can be determined before
build commands are issued.
