module;

#include <fmt/core.h>

export module mLogger;

export import <source_location>;
//export import <numeric>;

namespace invictus {

export namespace logger {

struct location_string {
	const char * format;
	std::source_location location;

	location_string(const char* format,
	                const std::source_location& location = std::source_location::current()) :
		format(format),
		location(location) {
	}
};

void debug(std::string_view format,
           fmt::format_args args,
           const std::source_location &location)
{
	fmt::print(stdout, "DEBUG {}({}): ", location.file_name(), location.line());
	fmt::vprint(stdout, format, args);
}

template<typename ...Args>
void DEBUG(const location_string &ls,
           const Args & ... args) {
	logger::debug(ls.format, fmt::make_format_args<fmt::format_context>(args...), ls.location);
}


void info(std::string_view format,
          fmt::format_args args,
          const std::source_location &location)
{
	fmt::print(stdout, "INFO {}({}): ", location.file_name(), location.line());
	fmt::vprint(stdout, format, args);
}

template<typename ...Args>
void INFO(const location_string &ls,
          const Args & ... args) {
	logger::info(ls.format, fmt::make_format_args<fmt::format_context>(args...), ls.location);
}


void warning(std::string_view format,
             fmt::format_args args,
             const std::source_location &location)
{
	fmt::print(stdout, "WARNING {}({}): ", location.file_name(), location.line());
	fmt::vprint(stdout, format, args);
}

template<typename ...Args>
void WARNING(const location_string &ls,
             const Args & ... args) {
	logger::info(ls.format, fmt::make_format_args<fmt::format_context>(args...), ls.location);
}


void error(std::string_view format,
           fmt::format_args args,
           const std::source_location &location)
{
	fmt::print(stdout, "ERROR {}({}): ", location.file_name(), location.line());
	fmt::vprint(stdout, format, args);
}

template<typename ...Args>
void ERROR(const location_string &ls,
           const Args & ... args) {
	logger::info(ls.format, fmt::make_format_args<fmt::format_context>(args...), ls.location);
}

	       void error(const char *format,
           fmt::format_args args,
           const std::source_location &location)
{
	fmt::print(stdout, "ERROR {}({}): ", location.file_name(), location.line());
	fmt::vprint(stderr, format, args);
}


void todo(std::string_view format,
          fmt::format_args args,
          const std::source_location &location)
{
	fmt::print(stdout, "TODO {}({}): ", location.file_name(), location.line());
	fmt::vprint(stdout, format, args);
}

template<typename ...Args>
void TODO(const location_string &ls,
          const Args & ... args) {
	logger::info(ls.format, fmt::make_format_args<fmt::format_context>(args...), ls.location);
}

} // namespace logger

} // namespace invictus

export namespace LOG = invictus::logger;
