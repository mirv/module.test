module;

#include <iostream>
#include <string_view>

export module hello;

export void hello(std::string_view const &name)
{
	std::cout << "G'day " << name << "!\n";
}
