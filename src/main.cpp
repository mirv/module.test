import <array>;

import hello;
import mLogger;

int main(void)
{
	hello("world");

	std::array<float, 15> test_array;
	for (int i = 0; i < 15; ++i) {
		test_array[i] = static_cast<float>(i);
		LOG::INFO("test_array[{}] is {}\n", i, test_array[i]);
	}

	return 0;
}
