import sys
import glob
import os
import logging
import multiprocessing
import tarfile
from pathlib import Path

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

# Set -j option to be the cpu count by default.
SetOption("num_jobs", multiprocessing.cpu_count())
logging.info("building with -j" + str(GetOption("num_jobs")))
# Cache the checksum if the file is older than 8 seconds.
SetOption("max_drift", 8)

top_path = Path(Dir('#').path)
bin_path = top_path / "bin"

buildmode = ARGUMENTS.get('mode', 'debug')
buildarch = ARGUMENTS.get('arch', 'x86_64')

env = Environment(ENV = os.environ)

env.Tool("compilation_db")
env.Alias("compiledb", env.CompilationDatabase('compile_commands.json'))

# Setup a gcc version that supports modules.
gcc_version = "-12.0.0"
env["CC"] = "gcc" + gcc_version
env["CXX"] = "g++" + gcc_version
env["LINK"] = "gcc" + gcc_version

builddir = "build"

modules_env = env.Clone()
modules_build_vars = {
    "buildmode" : buildmode,
    "env" : modules_env,
    "builddir" : builddir
}

SConscript(os.path.join("src", "SConscript"),
           variant_dir=builddir,
           duplicate=0,
           exports=modules_build_vars)
